// CanteenList.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3

ScrollView {
    id: scrollView
    anchors.fill: parent

    property bool loading: true
    property bool error: false
    property string errorText: ""
    property var canteenList: []
    signal update();
    Component.onCompleted: update()

    ListView {
        id: canteenListView
        anchors.fill: parent
        model: canteenList

        focus: true

        Label {
            width: parent.width
            anchors.centerIn: parent
            linkColor: UbuntuColors.orange
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            text: errorText
            visible: error && !loading
        }

        ActivityIndicator {
            anchors.centerIn: parent
            running: loading
        }

        delegate: ListItem {
            onClicked: {
                console.log("creating page");
                console.log("title is " + modelData.name);
                console.log("id is " + modelData.id);

                pageStack.push(Qt.resolvedUrl("../Mensa.qml"), {
                    name: modelData.name,
                    mensaId: modelData.id,
                    city: modelData.city,
                    address: modelData.address,
                    latitude: modelData.coordinates[0],
                    longitude: modelData.coordinates[1]
                });
            }

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        text: i18n.tr("Star")
                        checkable: true
                        checked: favourites.indexOf(modelData.id) != -1
                        onTriggered: {
                            console.log(favourites)
                            if (favourites.indexOf(modelData.id) == -1) {
                                favourites.push(modelData.id)
                            } else {
                                favourites.splice(favourites.indexOf(modelData.id),1);
                            }
                            console.log(favourites)
                        }
                        iconName: checked ? "starred" : "non-starred";
                    }
                ]
            }

            ListItemLayout {
                anchors.centerIn: parent

                title.text: modelData.name
                subtitle.text: modelData.address

                Icon {
                    width: units.gu(2); height: width
                    name: "go-next"
                    SlotsLayout.position: SlotsLayout.Last
                }
            }
        }
    }
}
